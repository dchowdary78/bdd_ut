package steps;



import com.fargo.cvsImpl.Drug;
import com.fargo.cvsImpl.Search;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class SearchSteps {
	Drug drug;
	Search search;
	
	@Given("I have a search field on the CVS Pharmacy page")
	public void i_have_a_search_field_on_the_cvs_pharmacy_page() {
	    System.out.println("S1.I am on CVS pharmacy page");
	}

	@When("I search for a product with name {string} and price {double}")
	public void i_search_for_a_product_with_name_and_price(String name, Double price) {
//	    System.out.println("S2.Search product with a name "+name+ " price is "+price);
	    
	    drug =new Drug(name,price);
//	    String name0=drug.getName();
//	    System.out.println("name0 "+drug.getName());
//	    Assert.assertEquals(drug.getName(), name);
	}

	@Then("Product with name {string} should displayed")
	public void product_with_name_should_displayed(String name) {
//		System.out.println("S3. product with "+name+" is displayed");
		search=new Search();
		String name1=search.displayProduct(drug);
//		System.out.println("name "+name1);
		Assert.assertEquals(drug.getName(), name1);
	    
	}
	
	


}
