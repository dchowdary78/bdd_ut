package steps;

import static org.junit.Assert.assertEquals;

//import org.junit.Assert;

import com.fargo.cvsImpl.Calculator;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class CalcSteps {
	
	Calculator calculator=new Calculator();
	
	@Given("I have a Caculator")
	public void i_have_a_caculator() {

	}

	@When("I add {int} and {int}")
	public void i_add_and(int int1, int int2) {
//		calculator=new Caculator();
		calculator.add(int1,int2);
	}
	
	@When("I substract {int} and {int}")
	public void i_substract_and(Integer int1, Integer int2) {
//	   calculator=new Caculator();
	   calculator.substract(int1,int2);
			   
	}

	@Then("The result should be {int}")
	public void the_result_should_be(int int1) {
		assertEquals(calculator.getResult(),int1);
	}

	


}
