package com.fargo.cvsImpl;

import java.util.ArrayList;
import java.util.List;

public class Drug {
	
	private String name;
	private double price;
	
	
	public Drug(String name, double price) {
		this.name = name;
		this.price = price;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
public List<String> getDrugList(){
	
	List<String> drugList =new ArrayList<>();
			drugList.add("Tylenol");
			drugList.add("Metamucil");
			drugList.add("Paracetomol");
	return drugList;
	
}
	
}
