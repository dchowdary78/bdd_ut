package com.fargo.cvsImpl;

public class Calculator {
	private int result;

	public int getResult() {
		return result;
	}
	
//	public void setResult(int result) {
//		this.result = result;
//	}

	public long add(int arg1,int arg2) {
		return arg1+arg2;
	}
	
	public long substract(int arg1,int arg2) {
		return arg1-arg2;
	}

	public long multiply(int a, int b) {
		return a*b;
	}

	public double divide(int a, int b) {
		return a/b;
	}
}
